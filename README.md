# tabularray-doc-zh-cn
Tabularray新一代表格排版宏包使用手册翻译，欢迎大家批评指正。

#### 介绍
计划将[Tabularray新一代表格排版宏包使用手册](https://github.com/lvjr/tabularray)翻译成中文，以方便后期查阅和使用。

#### 说明
1. 为保持与原说明手册格式一致，仅在原`manual.sty`文档类中添加了`ctex`宏包的调用。
2. 仅在TeXLive2021+Ubuntu 20.04下用`latexmk -xelatex manual.tex`进行了编译，不保证其它发行版和操作平台的编译正确性。
3. 由于Tabularray宏包开发目前非常活跃，其代码和手册一直在更新完善，为翻译方便，自2021M版本后，本翻译文档不再更新跟踪其英文文档，仅根据最新说明进行翻译。如有需要，请查阅其宏包使用手册原文。
4. 根据原说明，使用`lualatex`编译速度应该更快。

### 贡献
由于表格排版中的术语和概念不完全熟悉，且受能力所限，无保证翻译的准确性和合理性。如果您有任何改进意见或者功能需求，欢迎提交 [issue](https://gitee.com/nwafu_nan/tabularray-doc-zh-cn/issues) 或 [pull request](https://gitee.com/nwafu_nan/tabularray-doc-zh-cn/pulls)。



